#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <signal.h>
#include <fcntl.h>
#include <sys/wait.h>


#define BUFSZ 50000
#define HDSZ 8

int readline(int fd,char *ptr,int maxlen)
{
	int n, rc;
	char c;
	*ptr = 0;
	for(n=1; n<maxlen; n++)
	{
		rc=read(fd,&c,1);
		if(rc== 1)
		{
			*ptr++ = c;
			if(c=='\n')	break;
		}
		else if(rc==0)
		{
			if(n==1)		 return 0;
			else				 break;
		}
		else return(-1);
	}
	return n;
}	


int socket_handler(int browser_fd)
{
	char buf[BUFSZ];
	char request[1000];
	char package[HDSZ];
	struct sockaddr_in web_server_addr;
	FILE* firewall_config;

	bzero(request,sizeof(request));

	read(browser_fd,request,sizeof(request));
		

	unsigned char VN = request[0] ;
	unsigned char CD = request[1] ;
	unsigned int DST_PORT = (request[2] << 8)&0x0000FF00 | request[3]&0x000000FF ;
	unsigned int DST_IP = request[7] << 24 | (request[6] << 16)& 0x00FF0000 | (request[5] << 8)&0x0000FF00 | request[4]&0x000000FF ;
	char* USER_ID = request + 8 ;

	printf("-------\n");
	printf("VN=%u CD=%u(%s) USER_ID=%s \n",VN,CD,(CD==0x01)?"CONNECT":"BIND",USER_ID);
	//printf("Source Addr = %s(%u) \n", inet_ntoa());
	printf("Destination Addr = %u.%u.%u.%u(%u)\n", request[4]&0x000000FF,request[5]&0x000000FF,request[6]&0x000000FF,request[7]&0x000000FF,DST_PORT);
	printf("-------\n");
	
	firewall_config = fopen("socks.conf","r");

	char msg_buf[5000];
	int permit = 0;
	
	while(1)
	{
		char ip1[30],ip2[30],ip3[30],ip4[30];
		int ip_addr[4];
		char mode[50];
		char socks_type[10];
		bzero(msg_buf,sizeof(msg_buf));
		int len = readline(fileno(firewall_config),msg_buf,sizeof(msg_buf));
		if(len<5) break;
		for(int i=0;i<strlen(msg_buf);i++)
		{
			if(msg_buf[i] == '.') msg_buf[i] = ' ';
		}
		sscanf(msg_buf,"%s %s %s %s %s %s",mode,socks_type,ip4,ip3,ip2,ip1);
		
		ip_addr[3] = atoi(ip4);
		ip_addr[2] = atoi(ip3);
		ip_addr[1] = atoi(ip2);
		ip_addr[0] = atoi(ip1);

		//printf("RULE : %d.%d.%d.%d\n", ip_addr[3],ip_addr[2],ip_addr[1],ip_addr[0]);
		if(CD == 0x01 && strncmp(socks_type,"c",1) == 0)
		{
			
			if( ((request[4]&0x000000FF) == ip_addr[3] ||  ip_addr[3] == 0) && ((request[5]&0x000000FF) == ip_addr[2] ||  ip_addr[2] == 0) &&((request[6]&0x000000FF) == ip_addr[1] ||  ip_addr[1] == 0) && ((request[7]&0x000000FF) == ip_addr[0] ||  ip_addr[0] == 0))
			{

				package[1] = (unsigned char) 0x5A;
				break;
			}
			else
			{
				
				package[1] = (unsigned char) 0x5B;
			}
		}
		else if(CD == 0x02 && strncmp(socks_type,"b",1) == 0)
		{
			if( ((request[7]&0x000000FF) == ip_addr[3] ||  ip_addr[3] == 0) && ((request[6]&0x000000FF) == ip_addr[2] ||  ip_addr[2] == 0) && ((request[5]&0x000000FF) == ip_addr[1] ||  ip_addr[1] == 0) && ((request[4]&0x000000FF) == ip_addr[0] ||  ip_addr[0] == 0))
			{
				package[1] = (unsigned char) 0x5A;
				break;
			}
			else package[1] = (unsigned char) 0x5B;
		}
		else package[1] = (unsigned char) 0x5B;
	}
	
	printf("CD : %d\n", package[1]);
	if(CD == 0x01)
	{
		printf("connect mode\n");
		package[0] = 0;
		
		package[2] = request[2];
		package[3] = request[3];
		package[4] = request[4];
		package[5] = request[5];
		package[6] = request[6];
		package[7] = request[7];
		
		write(browser_fd,package,8);
		
		int web_fd;
		web_fd = socket(AF_INET,SOCK_STREAM,0);
		bzero((char *)&web_server_addr,sizeof(web_server_addr));
		web_server_addr.sin_family = AF_INET;
		web_server_addr.sin_addr.s_addr = DST_IP;
		web_server_addr.sin_port = htons(DST_PORT);
		

		
		//fcntl(web_fd, F_SETFL, fcntl(web_fd, F_GETFL, 0) | O_NONBLOCK);
		if (connect(web_fd,(struct sockaddr *)&web_server_addr,sizeof(web_server_addr)) < 0) printf("CONNECT DEST ERROR\n");;

		
		fd_set rfds, afds;
		int nfds = getdtablesize();

		FD_ZERO(&afds);
		FD_ZERO(&rfds);
		FD_SET(web_fd,&afds);
		FD_SET(browser_fd,&afds);
		char msg[BUFSZ];
		bzero(msg,sizeof(msg));

		while(1)
		{
			
			memcpy(&rfds,&afds,sizeof(rfds));
			if(select(nfds,&rfds,NULL,NULL,NULL) < 0)
			{
				printf("select fail\n");
				exit(1);
			}

			if(FD_ISSET(browser_fd,&rfds))
			{
				bzero(buf,sizeof(buf));

				int len = read(browser_fd,buf,sizeof(buf));
				

				if(len > 0)
				{
					len = write(web_fd,buf,len);
					if(len < 0) printf("!!!!!!WRITE ERROR!!!!!\n");
				}
			}
			
			if(FD_ISSET(web_fd,&rfds))
			{
				bzero(buf,sizeof(buf));
		
				int len = read(web_fd,buf,sizeof(buf));
				
				if(len > 0)
				{
					
					len = write(browser_fd,buf,len);
				}
			}
		}
	}
	else if(CD = 0x02)
	{
		int bindfd;
		struct sockaddr_in bind_addr;

		bindfd = socket(AF_INET,SOCK_STREAM,0);
		bind_addr.sin_family = AF_INET;
		bind_addr.sin_addr.s_addr = htonl(INADDR_ANY);
		bind_addr.sin_port = htons(INADDR_ANY);		

		if(bind(bindfd,(struct sockaddr *)&bind_addr,sizeof(bind_addr)) < 0)
		{
			printf("bind error\n");
			exit(3);
		}

		int z,sa_len;
		struct sockaddr_in sa;
		sa_len = sizeof(sa);
		z = getsockname(bindfd,(struct sockaddr *)&sa,&sa_len);
		if(z == -1) printf("getsockname error\n");

		if(listen(bindfd,5) < 0) exit(3);

		package[0] = 0;
		
		package[2] = (unsigned char)(ntohs(sa.sin_port)/256);
		package[3] = (unsigned char)(ntohs(sa.sin_port)%256);
		package[4] = 0;
		package[5] = 0;
		package[6] = 0;
		package[7] = 0;

		write(browser_fd,package,8);

		int ftp_fd;
		struct sockaddr_in ftp_addr;
		size_t length = sizeof(ftp_addr);
		if((ftp_fd = accept(bindfd,(struct sockaddr *)&ftp_addr,(socklen_t *)&length))<0)
		{
			printf("accept error\n");
			exit(3);
		}

		write(browser_fd,package,8);

		fd_set rfds, afds;
		int nfds = ((browser_fd<ftp_fd)?ftp_fd:browser_fd)+1;
		FD_ZERO(&afds);
		FD_ZERO(&rfds);
		FD_SET(ftp_fd,&afds);
		FD_SET(browser_fd,&afds);

		while(1)
		{
			
			memcpy(&rfds,&afds,sizeof(rfds));
			if(select(nfds,&rfds,NULL,NULL,NULL) < 0)
			{
				printf("select fail\n");
				exit(1);
			}

			if(FD_ISSET(browser_fd,&rfds))
			{
				bzero(buf,sizeof(buf));

				int len = read(browser_fd,buf,sizeof(buf));
				

				if(len > 0)
				{
					//printf("browser msg len : %d\n", len);
					//printf("browser msg : %s\n", buf);
					len = write(ftp_fd,buf,len);
					if(len < 0) printf("!!!!!!WRITE ERROR!!!!!\n");
				}
			}
			
			if(FD_ISSET(ftp_fd,&rfds))
			{
				bzero(buf,sizeof(buf));
		
				int len = read(ftp_fd,buf,sizeof(buf));
				
				if(len > 0)
				{
					//printf("web msg len : %d\n", len);
					//printf("web msg: %s\n", buf);
					len = write(browser_fd,buf,len);
				}
			}
		}
	}
	
	return 0;
}

int main()
{
	int listenfd, client_fd;
	int port = 7878;
	struct sockaddr_in proxy_addr,client_addr;

	if( (listenfd=socket(AF_INET,SOCK_STREAM,0)) < 0)
		printf("socket error\n");

	proxy_addr.sin_family = AF_INET;
	proxy_addr.sin_addr.s_addr = htonl(INADDR_ANY);
	proxy_addr.sin_port = htons(port);

	int enable = 1;
    if (listenfd < 0 || setsockopt(listenfd,SOL_SOCKET,SO_REUSEADDR,&enable,sizeof(int))<0) 
        printf("ERROR opening socket");

    if(bind(listenfd,(struct sockaddr *)&proxy_addr,sizeof(proxy_addr)) < 0)
    	printf("bind error\n");

    if(listen(listenfd,64)<0)
    	printf("listen error\n");

    char client_info[1000];
    while(1)
    {
    	int pid;
    	int len = sizeof(client_addr);
    	if( (client_fd=accept(listenfd,(struct sockaddr *)&client_addr,(socklen_t*)&len)) < 0)
    	{
    		exit(3);
    		printf("accept error\n");
    	}

    	sprintf(client_info,"%s",inet_ntoa(client_addr.sin_addr));

    	if((pid=fork())<0)
    		printf("fork error\n");
    	else if(pid == 0)
    	{
    		close(listenfd);
    		int c = socket_handler(client_fd);

    		if(c == -1) printf("ERROR\n");
    	}
    	else
    	{
    		close(client_fd);
		}
    }
}