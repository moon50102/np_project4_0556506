#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <signal.h>
#include <fcntl.h>

#define CONNMAX 5
#define BYTES 1024

char *ROOT = "/net/gcs/105/0556506/public_html";
int listenfd, client_fd;
void error(char *);
void startServer(char *);

int main(int argc, char* argv[])
{
    struct sockaddr_in clientaddr;
    socklen_t addrlen;
    char c;    
    
    //Default Values PATH = ~/ and PORT=10000
    char PORT[6];
    //ROOT = getenv("PWD");
    strcpy(PORT,"7878");

    int slot=0;

    
    printf("Server started at port no. %s\n",PORT);
    // Setting all elements to -1: signifies there is no client connected
    int i;
    
    client_fd=-1;
    startServer(PORT);

    // ACCEPT connections
    while (1)
    {
        addrlen = sizeof(clientaddr);
        client_fd = accept (listenfd, (struct sockaddr *) &clientaddr, &addrlen);

        if (client_fd<0)
            error ("accept() error");
        else
        {
            char mesg[99999], *reqline[3], data_to_send[BYTES], path[99999];
            int rcvd, fd, bytes_read;

            memset( (void*)mesg, (int)'\0', 99999 );

            rcvd=recv(client_fd, mesg, 99999, 0);

            if (rcvd<0)    // receive error
                fprintf(stderr,("recv() error\n"));
            else if (rcvd==0)    // receive socket closed
                fprintf(stderr,"Client disconnected upexpectedly.\n");
            else    // message received
            {
                printf("%s", mesg);
                reqline[0] = strtok (mesg, " \t\n");
                if ( strncmp(reqline[0], "GET\0", 4)==0 )
                {
                    reqline[1] = strtok (NULL, " \t");
                    reqline[2] = strtok (NULL, " \t\n");
                    if ( strncmp( reqline[2], "HTTP/1.0", 8)!=0 && strncmp( reqline[2], "HTTP/1.1", 8)!=0 )
                    {
                        write(client_fd, "HTTP/1.0 400 Bad Request\n", 25);
                    }
                    else
                    {
                        int fd;
                        int cgi_page = 0;
                        int get_param = 0;
                        char* cgi_arg[2];

                        for(int i=0;i<strlen(reqline[1]);++i)
                        {
                            if(reqline[1][i] == '?')
                            {
                                get_param = 1;
                                if(reqline[1][i-3] == 'c' && reqline[1][i-2] == 'g' && reqline[1][i-1] == 'i') cgi_page = 1;
                                break;
                            }
                        }

                        if(reqline[1][strlen(reqline[1])-3] == 'c' && reqline[1][strlen(reqline[1])-2] == 'g' && reqline[1][strlen(reqline[1])-1] == 'i') cgi_page = 1;

                        printf("%d\n", cgi_page);
                        if(get_param == 0)
                        {
                            strcpy(path,ROOT);
                            strcat(path, reqline[1]);

                            cgi_arg[0] = reqline[1];
                            cgi_arg[1] = NULL;

                            for(int i=0;i<strlen(cgi_arg[0])-1;++i)
                                cgi_arg[0][i] = cgi_arg[0][i+1];
                        }
                        else
                        {
                            char* s1 = strtok(reqline[1],"?");
                            char* s2 = strtok(NULL," ");
                            
                            strcpy(path,ROOT);
                            strcat(path, s1);

                            setenv("QUERY_STRING",s2,1);
                            

                            cgi_arg[0] = s1;
                            cgi_arg[1] = NULL;

                            for(int i=0;i<strlen(cgi_arg[0])-1;++i)
                                cgi_arg[0][i] = cgi_arg[0][i+1];
                            cgi_arg[0][strlen(cgi_arg[0])-1] = '\0';
                            
                        }
                        
                        printf("file: %s\n", path);

                        if(cgi_page == 0)
                        {
                            if ( (fd=open(path, O_RDONLY))!=-1 )    //FILE FOUND
                            {
                                send(client_fd, "HTTP/1.0 200 OK\n\n", 17, 0);
                                while ( (bytes_read=read(fd, data_to_send, BYTES))>0 )
                                {
                                    write (client_fd, data_to_send, bytes_read);
                                    printf("%s\n", data_to_send);
                                }
                            }
                            else printf("OPEN ERROR\n");
                        }
                        else if(cgi_page == 1)
                        {
                            send(client_fd, "HTTP/1.0 200 OK\n", 17, 0);
                            
                            pid_t pid;

                            pid = fork();

                            if(pid<0) 
                            {
                                fprintf(stderr, "Fork Failed");
                                exit(-1);

                            }
                            else if(pid==0) //child process
                            {

                                dup2(client_fd, STDOUT_FILENO);
                                
                                execv(path,cgi_arg);
                                
                            }
                        }
                        
                        close(client_fd);
                    }
                }
            }
        }
    }

    return 0;
}

//start server
void startServer(char *port)
{
    struct addrinfo hints, *res, *p;

    // getaddrinfo for host
    memset (&hints, 0, sizeof(hints));
    hints.ai_family = AF_INET;
    hints.ai_socktype = SOCK_STREAM;
    hints.ai_flags = AI_PASSIVE;
    if (getaddrinfo( NULL, port, &hints, &res) != 0)
    {
        perror ("getaddrinfo() error");
        exit(1);
    }
    // socket and bind
    for (p = res; p!=NULL; p=p->ai_next)
    {
        listenfd = socket (p->ai_family, p->ai_socktype, 0);
        int enable = 1;
        if (listenfd < 0 || setsockopt(listenfd,SOL_SOCKET,SO_REUSEADDR,&enable,sizeof(int))<0) 
            error("ERROR opening socket");
        if (listenfd == -1) continue;
        if (bind(listenfd, p->ai_addr, p->ai_addrlen) == 0) break;
    }
    if (p==NULL)
    {
        perror ("socket() or bind()");
        exit(1);
    }

    freeaddrinfo(res);

    // listen for incoming connections
    if ( listen (listenfd, 1000000) != 0 )
    {
        perror("listen() error");
        exit(1);
    }
}

//client connection
void respond()
{
    char mesg[99999], *reqline[3], data_to_send[BYTES], path[99999];
    int rcvd, fd, bytes_read;

    memset( (void*)mesg, (int)'\0', 99999 );

    rcvd=recv(client_fd, mesg, 99999, 0);

    if (rcvd<0)    // receive error
        fprintf(stderr,("recv() error\n"));
    else if (rcvd==0)    // receive socket closed
        fprintf(stderr,"Client disconnected upexpectedly.\n");
    else    // message received
    {
        printf("%s", mesg);
        reqline[0] = strtok (mesg, " \t\n");
        if ( strncmp(reqline[0], "GET\0", 4)==0 )
        {
            reqline[1] = strtok (NULL, " \t");
            reqline[2] = strtok (NULL, " \t\n");
            if ( strncmp( reqline[2], "HTTP/1.0", 8)!=0 && strncmp( reqline[2], "HTTP/1.1", 8)!=0 )
            {
                write(client_fd, "HTTP/1.0 400 Bad Request\n", 25);
            }
            else
            {
                if ( strncmp(reqline[1], "/\0", 2)==0 )
                    reqline[1] = "/index.html";        //Because if no file is specified, index.html will be opened by default (like it happens in APACHE...

                strcpy(path, ROOT);
                strcpy(&path[strlen(ROOT)], reqline[1]);
                printf("file: %s\n", path);

                if ( (fd=open(path, O_RDONLY))!=-1 )    //FILE FOUND
                {
                    send(client_fd, "HTTP/1.0 200 OK\n\n", 17, 0);
                    while ( (bytes_read=read(fd, data_to_send, BYTES))>0 )
                        write (client_fd, data_to_send, bytes_read);
                }
                else    write(client_fd, "HTTP/1.0 404 Not Found\n", 23); //FILE NOT FOUND
            }
        }
    }

    //Closing SOCKET
    shutdown (client_fd, SHUT_RDWR);         //All further send and recieve operations are DISABLED...
    close(client_fd);
    client_fd=-1;
}